https://bitbucket.org/CAA-eProd/cid-5x__caa-insurance-website

Backend Dev notes:

- All of the JavaScripts provided are for
demonstration/viewing purposes only and not
intended for production use. Use at your own risk!
- Support is only for the HTML and CSS.
- HTML templates can be found under src/components/
- The original direction was to use Playground's code which can be found here: https://github.com/playgroundinc/caa. Unfortunately, that changed mid project due to the bugs found, so I am now using only what seems to be working from their codebase, my own custom rewritten code and some code from other CAASCO projects.

- the only page that is coded so far can be previewed http://localhost:8080/#!/buy


General Notes:

- To run the project locally you will need Nodejs https://nodejs.org/en/
- To preview locally, in your terminal run "npm install" to install dependencies and then "npm start" in the /CODE directory. Project will run on port 8080 (localhost:8080).
- add modules to both index.js and webpack.config.js

