const path = require('path'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      ExtractTextPlugin = require('extract-text-webpack-plugin'),
      CleanWebpackPlugin = require('clean-webpack-plugin'),
      CopyWebpackPlugin = require('copy-webpack-plugin'),
      fs = require('fs'),
      ImageminPlugin = require('imagemin-webpack-plugin').default;

//the path(s) that should be cleaned
let pathsToClean = [
  'build/css.js', 'build/template.js','build/components.js','build/components/'
]

// the clean options to use
let cleanOptions = {
  exclude:  ['index.js'],
  verbose:  true,
  dry:      false,
  watch:    false
}


module.exports = {
    entry:  {
      index:  [
        "./src/index.js",
        "./src/app.config.js",
        "./src/components/header-section/header-section.module.js",
        "./src/components/header-section/header-section.component.js",
        "./src/components/nav-section/nav-section.module.js",
        "./src/components/nav-section/nav-section.component.js",
        "./src/components/footer-section/footer-section.module.js",
        "./src/components/footer-section/footer-section.component.js",
        "./src/components/header-description/header-description.module.js",
        "./src/components/header-description/header-description.component.js",
        "./src/components/description-section/description-section.module.js",
        "./src/components/description-section/description-section.component.js",
        "./src/components/hero-section/hero-section.module.js",
        "./src/components/hero-section/hero-section.component.js",
        "./src/components/broker-find/broker-find.module.js",
        "./src/components/broker-find/broker-find.component.js",
        "./src/components/broker-hero/broker-hero.module.js",
        "./src/components/broker-hero/broker-hero.component.js",
        "./src/components/buy-tabs/buy-tabs.module.js",
        "./src/components/buy-tabs/buy-tabs.component.js",
        "./src/components/property-tabs/property-tabs.module.js",
        "./src/components/property-tabs/property-tabs.component.js",
        "./src/svgs/svg-monitor/svg-monitor.module.js",
        "./src/svgs/svg-monitor/svg-monitor.component.js",
        "./src/svgs/svg-phone/svg-phone.module.js",
        "./src/svgs/svg-phone/svg-phone.component.js",
        "./src/svgs/svg-store/svg-store.module.js",
        "./src/svgs/svg-store/svg-store.component.js",
        "./src/components/feature-block/feature-block.module.js",
        "./src/components/feature-block/feature-block.component.js",
        "./src/components/broker-list/broker-list.module.js",
        "./src/components/broker-list/broker-list.component.js",
        "./src/components/store-list/store-list.module.js",
        "./src/components/store-list/store-list.component.js",
        "./src/components/faq-property/faq-property.module.js",
        "./src/components/faq-property/faq-property.component.js"
      ],
      template: './src/temp/template.js',
      css: './src/temp/css.js'
    },

    output: {
        filename: '[name].js',
        path: path.resolve(process.cwd(), 'build')
    },

    module: {
        loaders: [
            {
                test: /\.(jpeg|png|gif|svg)$/i,
                loader: [
                    'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ["es2015"]
                }
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                loader: "html-loader"
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',

                    // Could also be write as follow:
                    // use: 'css-loader?modules&localIdentName=[local]!postcss-loader'
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                localIdentName: '[local]'
                            }
                        },
                        'postcss-loader'
                    ]
                }),
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',

                    // Could also be write as follow:
                    // use: 'css-loader?modules&importLoader=2&sourceMap&localIdentName=[local]!sass-loader'
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                sourceMap: true,
                                importLoaders: 2,
                                localIdentName: '[local]'
                            }
                        },
                        'sass-loader'
                    ]
                }),
            },
        ],
    },

    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        new ExtractTextPlugin("style.css"),
        new HtmlWebpackPlugin({
            template: 'src/index.html', inject: true
        }),
        new HtmlWebpackPlugin({
            filename: 'home.html', template: 'src/home.html'
        }),
        new HtmlWebpackPlugin({
            filename: 'buy.html', template: 'src/buy.html'
        }),
        new HtmlWebpackPlugin({
            filename: 'property.html', template: 'src/property.html'
        }),
        new HtmlWebpackPlugin({
            filename: 'test.html', template: 'src/test.html'
        }),
        new CopyWebpackPlugin([
            // Copy directory contents to {output}/to/directory/
            { from: 'src/components', to: 'components' },
            { from: 'src/svgs', to: 'svgs' },
            { from: 'src/data', to: 'data' },
            { from: 'src/Fonts', to: 'Fonts' },
            { from: 'src/img', to: 'img' },
        ]),
        // new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i })
        new ImageminPlugin({
          disable: process.env.NODE_ENV !== 'production', // Disable during development
          pngquant: {
            quality: '95-100'
          }
        })
    ]
};