'use strict';

describe('brokerList', function() {

  // Load the module that contains the `brokerList` component before each test
  beforeEach(module('brokerList'));

  // Test the controller
  describe('brokerListController', function() {
    var $httpBackend, ctrl;

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service and assign it to a variable with the same name
    // as the service while avoiding a name conflict.
    beforeEach(inject(function($componentController, _$httpBackend_) {
      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('broker/broker.json')
                  .respond([{firstname: 'Apple'}, {firstname: 'Delta'}]);

      ctrl = $componentController('brokerList');
    }));

    it('should create a `broker` property with 2 broker fetched with `$http`', function() {
      expect(ctrl.broker).toBeUndefined();

      $httpBackend.flush();
      expect(ctrl.broker).toEqual([{name: 'Apple'}, {name: 'Delta'}]);
    });

    it('should set a default value for the `sortType` firstname', function() {
      expect(ctrl.sortType).toBe('firstname');
    });

  });

});