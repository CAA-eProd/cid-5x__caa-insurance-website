'use strict';

// Register `brokerList` component, along with its associated controller and template
angular.
  module('brokerList').
  component('brokerList', {
    templateUrl: 'components/broker-list/broker-list.template.html',
    controller: ['$http', function BrokerListController($http) {
      var self = this;
      self.showList = false;
      self.sortType  = 'firstname'; // set the default sort type
      self.sortReverse  = false;  // set the default sort order

      $http.get('data/broker.json').then(function(response) {
        self.broker = response.data;
      });

      self.toggle = false;
      self.toggleFilter = function(store) {
        store.toggle = !store.toggle;
      };

      self.limit = 3;

    }]
  });