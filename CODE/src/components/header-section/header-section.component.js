'use strict';

// Register `header` component, along with its associated controller and template
angular.
  module('headerSection').
  component('headerSection', {
    templateUrl: 'components/header-section/header-section.template.html',
    controller: ['$http', function HeaderController($http) {
      var self = this;
      self.orderProp = 'name';

      $http.get('data/hero.json').then(function(response) {
        self.hero = response.data;
      });
    }]
  });