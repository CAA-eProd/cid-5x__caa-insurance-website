'use strict';

// Register `heroSection` component, along with its associated controller and template
angular.
  module('heroSection').
  component('heroSection', {
    templateUrl: 'components/hero-section/hero-section.template.html',
    controller: function($element){
      this.header = $element.attr("header");
      this.description = $element.attr("description");
    }
  });