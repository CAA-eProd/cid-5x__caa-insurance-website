'use strict';

// Register `navSection` component, along with its associated controller and template
angular.
  module('navSection').
  component('navSection', {
    templateUrl: 'components/nav-section/nav-section.template.html',
    controller: ['$http', function NavSectionController($http, $digest) {
      var self = this;
      self.toggleNav = false;

      window.addEventListener("resize", function() {
        if (window.matchMedia("(min-width: 960px)").matches) {
            self.toggleNav = false;
            // self.$digest();
        }
      });


       self.toggleLight = false;
    }]
  });