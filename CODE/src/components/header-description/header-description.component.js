'use strict';

// Register `headerDescription` component, along with its associated controller and template
angular.
  module('headerDescription').
  component('headerDescription', {
    templateUrl: 'components/header-description/header-description.template.html',
    controller: function($element){
      this.header = $element.attr("header");
      this.description = $element.attr("description");
      // this.description = $sce.trustAsHtml(this.description);
    }
  });