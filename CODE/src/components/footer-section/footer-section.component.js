'use strict';

// Register `footerSection` component, along with its associated controller and template
angular.
  module('footerSection').
  component('footerSection', {
    templateUrl: 'components/footer-section/footer-section.template.html'
  });