'use strict';

// Register `storeList` component, along with its associated controller and template
angular.
  module('storeList').
  component('storeList', {
    templateUrl: 'components/store-list/store-list.template.html',
    controller: ['$http', function StoreListController($http) {
      var self = this;
      self.showList = false;
      self.sortType  = 'storename'; // set the default sort type
      self.sortReverse  = false;  // set the default sort order
      $http.get('data/store.json').then(function(response) {
        self.store = response.data;
      });


      self.toggle = false;
      self.toggleFilter = function(store) {
        store.toggle = !store.toggle;
        console.log(store.toggle);
      };

      self.limit = 3;


    }]
  });