'use strict';

// Register `descriptionSection` component, along with its associated controller and template
angular.
  module('descriptionSection').
  component('descriptionSection', {
    templateUrl: 'components/description-section/description-section.template.html',
    controller: function($element){
      this.description = $element.attr("description");
      // this.description = $sce.trustAsHtml(this.description);
    }
  });