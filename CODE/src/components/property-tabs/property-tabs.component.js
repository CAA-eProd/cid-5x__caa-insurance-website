'use strict';

// Register `buyTabs` component, along with its associated controller and template
angular.
  module('propertyTabs').
  component('propertyTabs', {
    templateUrl: 'components/property-tabs/property-tabs.template.html'
  });