'use strict';

// Register `brokerFind` component, along with its associated controller and template
angular.
  module('brokerFind').
  component('brokerFind', {
    templateUrl: 'components/broker-find/broker-find.template.html',
    controller: function($element, $sce){
      this.header = $element.attr("header");
      this.description = $element.attr("description");
    }
  });