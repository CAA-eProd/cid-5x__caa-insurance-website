'use strict';

// Register `faqProperty` component, along with its associated controller and template
angular.
  module('faqProperty').
  component('faqProperty', {
    templateUrl: 'components/faq-property/faq-property.template.html',
    controller: ['$http', '$timeout', function FaqPropertyController($http, $timeout) {
      var self = this;

      $http.get('data/faq-property.json').then(function(response) {
        self.faq = response.data;
      });

      self.toggle = false;
      self.toggleFilter = function(question) {
        closeOpenQuestion();
        question.toggle = !question.toggle;
      };

      function closeOpenQuestion() {
          var row = document.querySelector('.row');
          var ques = document.querySelector('.question-open');
          var qor = document.querySelector('.question-open + .row');
          var dnone = document.querySelector('.d-none');

          if (angular.element(row).hasClass(ques)) {
            console.log("has class");
            $timeout(function() {


              angular.forEach(self.faq.toggle, function(value, key){
                 self.faq.toggle = false;
              });

              //angular.element(ques).triggerHandler('click');
              angular.element(qor).addClass('d-none');
              angular.element(ques).removeClass('question-open');
              // ques = ques + '';
              // console.log('close' + ques);

            });
          }
        //

      }


      self.limit = 3;

    }]
  });