'use strict';

// Register `featureBlock` component, along with its associated controller and template
angular.
  module('featureBlock').
  component('featureBlock', {
    templateUrl: 'components/feature-block/feature-block.template.html',
    controller: function($element){
      this.image = $element.attr("image");
      this.header = $element.attr("header");
      this.description = $element.attr("description");
      // this.description = $sce.trustAsHtml(this.description);
    }
  });