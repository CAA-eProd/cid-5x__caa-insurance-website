'use strict';

// Register `buyTabs` component, along with its associated controller and template
angular.
  module('buyTabs').
  component('buyTabs', {
    templateUrl: 'components/buy-tabs/buy-tabs.template.html'
  });