'use strict';

// Register `svgPhone` component, along with its associated controller and template
angular.
  module('svgPhone').
  component('svgPhone', {
    templateUrl: 'svgs/svg-phone/svg-phone.template.html'
  });