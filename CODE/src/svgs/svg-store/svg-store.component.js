'use strict';

// Register `svgStore` component, along with its associated controller and template
angular.
  module('svgStore').
  component('svgStore', {
    templateUrl: 'svgs/svg-store/svg-store.template.html'
  });