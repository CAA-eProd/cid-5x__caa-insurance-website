'use strict';

angular.
  module('insuranceApp').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/', {
          templateUrl: 'home.html'
        }).
        when('/buy', {
          templateUrl: 'buy.html'
        }).
        when('/property', {
          templateUrl: 'property.html'
        }).
        when('/test', {
          templateUrl: 'test.html'
        }).
        otherwise('/');

      // $locationProvider.html5Mode(true);
    }
  ])