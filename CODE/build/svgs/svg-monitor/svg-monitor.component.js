'use strict';

// Register `svgMonitor` component, along with its associated controller and template
angular.
  module('svgMonitor').
  component('svgMonitor', {
    templateUrl: 'svgs/svg-monitor/svg-monitor.template.html'
  });